#!/usr/bin/env bash

CMDFILE=""

if [ $# -ne 1 ]; then
	echo "$# is not a correct number of arguments"
	echo "Usage: $(basename ${0}) CMDFILE"
	exit 2
fi

CMDFILE=${1}

F=$( echo $(basename ${CMDFILE}) | sed -e 's/\.sh$//' )  # remove .sh
rm -f ${F}__`hostname`.nohup

nohup ${CMDFILE} >> ${F}__`hostname`.nohup &
