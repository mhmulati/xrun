#!/usr/bin/env bash

EXPRUNDIRFLAGFILENAME=".EXPRUNDIR"

if [ $# -ne 0 -a $# -ne 1 ]; then
	echo "$# is not a correct number of arguments"
	echo "Usage: $(basename ${0}) [XRUNGROUP]"
	exit 2
fi

ORIGPWD=${PWD}

if [ ! -f ${EXPRUNDIRFLAGFILENAME} ]; then
	if [ ! -d run ]; then
		cd ${ORIGPWD}
		exit 0

		# echo "Error: Dir run does not exist"
		# cd ${ORIGPWD}
		# exit 2
	fi

	cd run

	if [ ! -f ${EXPRUNDIRFLAGFILENAME} ]; then
		echo "Error: File ${EXPRUNDIRFLAGFILENAME} does not exist"
		cd ${ORIGPWD}
		exit 2
	fi
fi

# EXPSETDIR=..

if [ $# = 1 ]; then
	XRUNGROUP=${1}
else
    XRUNGROUP=$( echo $(basename $(dirname ${PWD})) | sed -e 's/\.sh$//' ) # remove .sh extension, if exists
fi

xrungroupclear ${XRUNGROUP}

XRUNGROUPPATH=${XRUNGROUP}.xrungroup

# echo "#!/usr/bin/env bash" > ${XRUNGROUPPATH}

for RUNEXPID in $( ls  -v ${XRUNGROUP}*.sh -I *.xrungroup ); do  # https://stackoverflow.com/questions/7992689/bash-how-to-loop-all-files-in-sorted
	echo "processing '${RUNEXPID}'"
	cat ${RUNEXPID} >> ${XRUNGROUPPATH}
	# tail -n +2 ${RUNEXPID} >> ${XRUNGROUPPATH}
done

mv ${XRUNGROUPPATH} ${XRUNGROUP}.sh
chmod u+x ${XRUNGROUP}.sh

if [ -f ${XRUNGROUP}.sh ]; then
	echo "created '${XRUNGROUP}.sh'"
fi

cd ${ORIGPWD}
