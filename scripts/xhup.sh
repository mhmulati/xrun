#!/usr/bin/env bash

EXPRUNDIRFLAGFILENAME=.EXPRUNDIR
CMDFILE=./XRUNGROUP.sh
N=1
L=1
M="1G"
X=""
Y=""
Z=""

if [ $# -ne 0 -a $# -ne 1 -a $# -ne 2 -a $# -ne 3 -a $# -ne 4 -a $# -ne 5 -a $# -ne 6 -a $# -ne 7 ]; then
	echo "$# is not a correct number of arguments"
	echo "Usage: $(basename ${0}) [CMDFILE [[N [M [L [X [Y [Z]]]]]]]"
	echo "CMDFILE default is name of the current experiment set"
	echo "N is the number of logical processors to be used; 0 means unrestricted; default ${N}"
	echo "M is the free memory size necessary to start a new process, default ${M}"
	echo "L is the number of the line to start reading the file, beggining with 1, default ${L}"
	echo "X, Y, and Z are options to be passed to parallel program, eg, --resume, --resume-failed, or --retry-failed"
	echo "Note: this script must be called from a EXPRUNDIR. That directory must contains a (preferably empty) file named ${EXPRUNDIRFLAGFILENAME}"
	exit 2
fi

if [ $# -ge 1 ]; then
	CMDFILE=${1}
else
	CMDFILE="./$( echo $(basename $(dirname ${PWD})) | sed -e 's/\.sh$//' ).sh"
fi

if [ $# -ge 2 ]; then
	N=${2}
fi

if [ $# -ge 3 ]; then
	M=${3}
fi

if [ $# -ge 4 ]; then
	L=${4}
fi

if [ $# -ge 5 ]; then
	X=${5}
fi

if [ $# -ge 6 ]; then
	Y=${6}
fi

if [ $# -ge 7 ]; then
	Z=${7}
fi

if [ ! -f ${EXPRUNDIRFLAGFILENAME} ]; then
	echo "Error: File ${EXPRUNDIRFLAGFILENAME} does not exist"
	exit 2
fi

~/bin/parall ${CMDFILE} ${N} ${M} ${L} ${X} ${Y} ${Z}
