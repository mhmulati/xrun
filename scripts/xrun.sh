#!/usr/bin/env bash

CMDFILE=./XRUNGROUP.sh
N=1
M="1G"
X=""
Y=""
Z=""

if [ $# -ne 0 -a $# -ne 1 -a $# -ne 2 -a $# -ne 3 -a $# -ne 4 -a $# -ne 5 -a $# -ne 6 ]; then
	echo "$# is not a correct number of arguments"
	echo "Usage: $(basename ${0}) [CMDFILE [[N [M [X [Y [Z]]]]]]"
	echo "CMDFILE default is current experiment set name"
	echo "N is the number of logical processors to be used; 0 means unrestricted; default ${N}"
	echo "M is the free memory size necessary to start a new process, default ${M}"
	echo "X, Y, and Z are options to be passed to parallel program, eg, --resume, --resume-failed, or --retry-failed"
	exit 2
fi

if [ $# -ge 1 ]; then
	CMDFILE=${1}
else
	CMDFILE="./$( echo $(basename $(dirname ${PWD})) | sed -e 's/\.sh$//' ).sh"
fi

if [ $# -ge 2 ]; then
	N=${2}
fi

if [ $# -ge 3 ]; then
	M=${3}
fi

if [ $# -ge 4 ]; then
	X=${4}
fi

if [ $# -ge 5 ]; then
	Y=${5}
fi

if [ $# -ge 6 ]; then
	Z=${6}
fi

F=$( echo $(basename ${CMDFILE}) | sed -e 's/\.sh$//' ) # remove .sh
if [ -f ${F}__`hostname`.nohup -a ! -z "${X}" ]; then
	echo -e "\n====== RUNNING AGAIN WITH ${X} ARG FOR PARALLEL ======" >> ${F}__`hostname`.nohup
else
	rm -f ${F}__`hostname`.nohup
fi

nohup ~/bin/xhup ${CMDFILE} ${N} ${M} 1 ${X} ${Y} ${Z} >> ${F}__`hostname`.nohup &
