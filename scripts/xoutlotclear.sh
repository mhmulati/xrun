#!/usr/bin/env bash

EXPSETDIRFLAGFILENAME=".EXPSETDIR"
EXPRUNDIRFLAGFILENAME=".EXPRUNDIR"

if [ $# -ne 0 ]; then
	echo "$# is not the correct number of arguments"
	echo "Usage: $(basename ${0})"
	exit 2
fi

ORIGPWD=${PWD}

if [ ! -f ${EXPSETDIRFLAGFILENAME} ]; then
	cd ..
	if [ ! -f ${EXPSETDIRFLAGFILENAME} ]; then
		echo "Error: File ${EXPSETDIRFLAGFILENAME} does not exist"
		cd ${ORIGPWD}
		exit 2
	fi
fi

EXPSETDIR=.

xchksumlotgroupclear

for EXPIDDIR in $( find out -mindepth 1 -maxdepth 1 -type d | sort ); do
	EXPID=$(basename ${EXPIDDIR})
	xoutclear ${EXPID}
done

if [ -d out ]; then
	if [ -z "$(ls -1qA out)" ]; then
		rmdir -v out
	fi
fi

if [ -d run ]; then
	rm -rfv run/$(basename $(readlink -f ${EXPSETDIR}))__*.joblog
	rm -rfv run/$(basename $(readlink -f ${EXPSETDIR}))__*.nohup
	rm -rfv run/$(basename $(readlink -f ${EXPSETDIR}))__*.numprocs

	if [ "$(ls -1qA run)" == "${EXPRUNDIRFLAGFILENAME}" ]; then
        rm -v run/${EXPRUNDIRFLAGFILENAME}
        rmdir -v run
    fi
fi

if [ -d ${ORIGPWD} ]; then
	cd ${ORIGPWD}
fi

