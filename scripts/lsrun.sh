#!/usr/bin/env bash

if [ $# -ne 1 ]
then
	echo "$# is not a correct number of arguments"
	echo "Usage:"
	echo "$(basename ${0}) PROCESSPATERN"
	exit 2
fi

PROCESSPATERN=${1}

ps -urx | grep -i ${PROCESSPATERN}
