#!/usr/bin/env bash

CMDFILE=${1}
N=0
L=2
M="0G"
X=""
Y=""
Z=""

if [ $# == 1 -a "${1} == -h" -o $# -ne 0 -a $# -ne 1 -a $# -ne 2 -a $# -ne 3 -a $# -ne 4 -a $# -ne 5 -a $# -ne 6 -a $# -ne 7 ]; then
	echo "$# is not a correct number of arguments"
	echo "Usage: $(basename ${0}) CMDFILE [N [L [M [X [Y [Z]]]]]]"
	echo "N is the number of logical processors to be used; 0 means unrestricted; default ${N}"
	echo "M is the free memory size necessary to start a new process, default ${M}"
	echo "L is the number of the line to start reading the file, beggining with 1, default ${L}"
	echo "X, Y, and Z are options to be passed to parallel program, eg, --resume, --resume-failed, or --retry-failed"
	exit 2
fi

if [ $# -ge 2 ]; then
	N=${2}
fi

if [ $# -ge 3 ]; then
	M=${3}
fi

if [ $# -ge 4 ]; then
	L=${4}
fi

if [ $# -ge 5 ]; then
	X=${5}
	# GAMBIARRA!
	if [ $X == 0 ]; then
		X=""
	fi
fi

if [ $# -ge 6 ]; then
	Y=${6}
	# GAMBIARRA!
	if [ $Y == 0 ]; then
		Y=""
	fi
fi

if [ $# -ge 7 ]; then
	Z=${7}
	# GAMBIARRA!
	if [ $Z == 0 ]; then
		Z=""
	fi
fi

F=$( echo $(basename ${CMDFILE}) | sed -e 's/\.sh$//' ) # remove .sh

if [ "${F} == ${CMDFILE}" -a ! -f ${CMDFILE} ]; then
	CMDFILE="${CMDFILE}.sh"
fi

if [ "${X}" != "--resume" ]; then
	rm -rf ${F}__`hostname`.joblog
	echo ${N} > ${F}__`hostname`.numprocs
elif [ ! -f ${F}__`hostname`.numprocs ]; then
	echo ${N} > ${F}__`hostname`.numprocs
fi

# fkm05: --gnu
tail -n +${L} ${CMDFILE} | parallel --jobs ${F}__`hostname`.numprocs --load 90% --memfree ${M} --keep-order --tmpdir ~/tmp --compress --joblog ${F}__`hostname`.joblog ${X} ${Y} ${Z} # --noswap --retries 1024 --delay 120
