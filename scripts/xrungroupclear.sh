#!/usr/bin/env bash

EXPRUNDIRFLAGFILENAME=".EXPRUNDIR"

if [ $# -ne 0 -a $# -ne 1 ]; then
	echo "$# is not a correct number of arguments"
	echo "Usage: $(basename ${0}) [XRUNGROUP]"
	echo "We are using one group per exp set dir"
	exit 2
fi

ORIGPWD=${PWD}

if [ ! -f ${EXPRUNDIRFLAGFILENAME} ]; then
	if [ ! -d run ]; then
		cd ${ORIGPWD}
		exit 0

		# echo "Error: Dir run does not exist"
		# cd ${ORIGPWD}
		# exit 2
	fi

	cd run

	if [ ! -f ${EXPRUNDIRFLAGFILENAME} ]; then
		echo "Error: File ${EXPRUNDIRFLAGFILENAME} does not exist"
		cd ${ORIGPWD}
		exit 2
	fi
fi

# EXPSETDIR=..

if [ $# = 1 ]; then
	XRUNGROUP=${1}
else
	XRUNGROUP=$( echo $(basename $(dirname ${PWD})) | sed -e 's/\.sh$//' ) # remove .sh extension, if exists
fi

rm -fv ${XRUNGROUP}.sh
rm -fv ${XRUNGROUP}.joblog
rm -fv ${XRUNGROUP}.nohup
rm -fv ${XRUNGROUP}.numprocs

cd ${ORIGPWD}
