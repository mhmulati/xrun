#!/usr/bin/env bash

EXPSETDIRFLAGFILENAME=".EXPSETDIR"

if [ $# -ne 1 ]; then
	echo "$# is not the correct number of arguments"
	echo "Usage: $(basename ${0}) EXPID"
	exit 2
fi

ORIGPWD=${PWD}

if [ ! -f ${EXPSETDIRFLAGFILENAME} ]; then
	cd ..
	if [ ! -f ${EXPSETDIRFLAGFILENAME} ]; then
		echo "Error: File ${EXPSETDIRFLAGFILENAME} does not exist"
		cd ${ORIGPWD}
		exit 2
	fi
fi

# EXPSETDIR=.

EXPID=$( basename ${1} )

EXPID=$( echo ${EXPID} | sed -e 's/\.meta\.conf\.yaml$//' ) # remove .meta.conf.conf extension, if present
EXPID=$( echo ${EXPID} | sed -e 's/\.sh$//' )               # remove .sh extension, if present

# set -v

if [ -d out ]; then
	rm -rfv out/${EXPID}
fi

if [ -d run ]; then
	rm -rfv run/${EXPID}__*.joblog
	rm -rfv run/${EXPID}__*.nohup
	rm -rfv run/${EXPID}__*.numprocs
fi

# set -

cd ${ORIGPWD}
