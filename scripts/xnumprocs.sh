#!/usr/bin/env bash

EXPRUNDIRFLAGFILENAME=".EXPRUNDIR"
NUMPROCSFILE="./XRUNGROUP__$(hostname).numprocs"
N=16

if [ $# -ne 2 ]; then
	echo "$# is not a correct number of arguments"
	echo "Usage: $(basename ${0}) NUMPROCSFILE N"
	echo "CMDFILE default is ${NUMPROCSFILE}"
	echo "N is the number of logical processors to be used; 0 means unrestricted; default ${N}"
	exit 2
fi

if [ $# -eq 2 ]; then
	NUMPROCSFILE=${1}
	N=${2}
fi

F=$( echo $(basename ${NUMPROCSFILE}) | sed -e 's/\.numprocs$//' ) # remove .numprocs
# F=$( echo $(basename ${NUMPROCSFILE}) | sed -e 's/\.[^.]*$//' ) # remove .numprocs
F_=$( echo ${F} | tr -e '__' )

# detect hostname, ie, x__hostname
# if hostname, do not change it
# if not hostname, append the local hostname to it
if [ ${#F_[@]} -eq 1 ]; then
	F="${F}__$(hostname)"
fi

F="${F}.numprocs"

if [ ! -f ${EXPRUNDIRFLAGFILENAME} ]; then
	echo "Error: File ${EXPRUNDIRFLAGFILENAME} does not exist"
	exit 2
fi

# LOW LEVEL PROCESSING
if [ ! -f ${F} ]; then
	echo "Error: File ${F} does not exist"
	exit 2
fi

if [ ! "${N}" =~ ^[0-9]+$ ]; then
	echo "Error: ${N} is not an integer"
	exit 2
fi

echo ${N} > ${F}
