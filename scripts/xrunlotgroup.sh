#!/usr/bin/env bash

EXPRUNDIRFLAGFILENAME=".EXPRUNDIR"

if [ $# -ne 0 ]; then
	echo "$# is not a correct number of arguments"
	echo "Usage: $(basename ${0})"
	echo "We are using one group per exp set dir"
	exit 2
fi

ORIGPWD=${PWD}

if [ ! -f ${EXPRUNDIRFLAGFILENAME} ]; then

	if [ "$(basename ${PWD})" = "in" ]; then
		cd ..
	fi

	if [ ! -d run ]; then
		cd ${ORIGPWD}
		exit 0

		# echo "Error: Dir run does not exist"
		# cd ${ORIGPWD}
		# exit 2
	fi

	cd run

	if [ ! -f ${EXPRUNDIRFLAGFILENAME} ]; then
		echo "Error: File ${EXPRUNDIRFLAGFILENAME} does not exist"
		cd ${ORIGPWD}
		exit 2
	fi
fi

# EXPSETDIR=..

XRUNGROUP=$( echo $(basename $(dirname ${PWD})) | sed -e 's/\.sh$//' ) # remove .sh extension, if exists

xrungroup ${XRUNGROUP}

cd ${ORIGPWD}
